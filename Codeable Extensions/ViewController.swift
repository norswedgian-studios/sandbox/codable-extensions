//
//  ViewController.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let goodJson = #"{"name":"Raw Json","age":22}"#
    let badJsonTypeMismatch = #"{"name":"Raw Json","age":"22"}"#
    let badJsonValueNotFound = #"{"name":null}"#
    let badJsonKeyNotFound = #"{"age":22}"#
    let badJsonDataCorrupted = #"{"name":+++}"#

    override func viewDidLoad() {
        super.viewDidLoad()

        createPersonFromJSONString()
    }
    
    private func createPersonFromJSONString() {
        print("\(#function)\n")
        
        print("Attempting to create from good JSON:\n\(goodJson)")
        if let person = PersonModel(fromJson: goodJson) {
            print("Successfully created person:\n\(person)")
        } else {
            print("Something went wrong")
        }
        print()
        
        print("Attempting to create from bad JSON (type mismatch):\n\(badJsonTypeMismatch)")
        if let person = PersonModel(fromJson: badJsonTypeMismatch) {
            print("Successfully created person:\n\(person)")
        } else {
            print("Something went wrong")
        }
        print()
        
        print("Attempting to create from bad JSON (value not found):\n\(badJsonValueNotFound)")
        if let person = PersonModel(fromJson: badJsonValueNotFound) {
            print("Successfully created person:\n\(person)")
        } else {
            print("Something went wrong")
        }
        print()
        
        print("Attempting to create from bad JSON (key not found):\n\(badJsonKeyNotFound)")
        if let person = PersonModel(fromJson: badJsonKeyNotFound) {
            print("Successfully created person:\n\(person)")
        } else {
            print("Something went wrong")
        }
        print()
        
        print("Attempting to create from bad JSON (data corrupted):\n\(badJsonDataCorrupted)")
        if let person = PersonModel(fromJson: badJsonDataCorrupted) {
            print("Successfully created person:\n\(person)")
        } else {
            print("Something went wrong")
        }
        print()
    }

}

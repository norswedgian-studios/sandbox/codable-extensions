//
//  Decodable+JSONString.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension Decodable {
    init?(fromJson json: String) {
        guard let data = json.data(using: .utf8) else {
            print("Unable to parse json to data object using utf8.\njson:\n\(json)")
            return nil
        }
        
        if trackDecodedObjects {
            guard let object: Self = Self(fromData: data) else { return nil }
            decodedObjects.add(DecodedObject(json: json, object: object))
            self = object
        } else {
            self.init(fromData: data)
        }
    }
}

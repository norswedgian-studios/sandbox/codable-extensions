//
//  PersonModel.swift
//  Codeable ExtensionsTests
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

struct PersonModel: Codable {
    var name: String
    var age: Int?
}

extension PersonModel: CustomStringConvertible {
    var description: String { return "name: \(name)\nage: \(age ?? 0)" }
}

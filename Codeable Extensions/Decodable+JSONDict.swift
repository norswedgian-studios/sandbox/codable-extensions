//
//  Decodable+JSONDict.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]

func ==(lhs: JSONDictionary, rhs: JSONDictionary ) -> Bool {
    return NSDictionary(dictionary: lhs).isEqual(to: rhs)
}

extension Decodable {
    init?(fromDict dict: JSONDictionary) {
        guard let data = try? JSONSerialization.data(withJSONObject: dict, options: []) else {
            print("Unable to parse dict to data object.\ndict:\n\(dict)")
            return nil
        }
                
        if trackDecodedObjects {
          guard let object: Self = Self(fromData: data) else { return nil }
          decodedObjects.add(DecodedObject(json: nil, object: object))
            self = object
        } else {
            self.init(fromData: data)
        }
    }
}

//
//  Decodable+JSONFile.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension Decodable {
    init?(fromFile file: String) {
        guard let filePath = Bundle.main.path(forResource: file, ofType: "json") else {
            print("Could not find file \"\(file).json\"")
            return nil
        }
        
        guard let json = try? String(contentsOfFile: filePath) else {
            print("Could not read contents of filepath \"\(filePath)\"")
            return nil
        }
        
        self.init(fromJson: json)
    }
}

//
//  Decodable+Data.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 9/4/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension Decodable {
    init?(fromData data: Data) {
        let object: Self
        do {
            object = try JSONDecoder().decode(Self.self, from: data)
        } catch DecodingError.typeMismatch(_, let context) {
            print("DecodingError.typeMismatch")
            print(context)
            return nil
        } catch DecodingError.valueNotFound(_, let context) {
            print("DecodingError.valueNotFound")
            print(context)
            return nil
        } catch DecodingError.keyNotFound(_, let context) {
            print("DecodingError.keyNotFound")
            print(context)
            return nil
        } catch DecodingError.dataCorrupted(let context) {
            print("DecodingError.dataCorrupted")
            print(context)
            return nil
        } catch {
            print("Unexpected decoding error")
            return nil
        }
        
        self = object
    }
}

//
//  Encodable+JSONDict.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension Encodable {
    var jsonDict: JSONDictionary? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        guard let dict = try? JSONSerialization.jsonObject(with: data) else { return nil }
        return dict as? JSONDictionary
    }
}

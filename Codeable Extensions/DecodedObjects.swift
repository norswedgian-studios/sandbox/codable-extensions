//
//  DecodedObjects.swift
//  Codeable Extensions
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

struct DecodedObject {
    var json: String?
    var object: Decodable
}

struct DecodedObjects {
    var decodedObjects: [DecodedObject]
    
    var types: [String] { return decodedObjects.map { "\(type(of: $0.object))" } }
}

extension DecodedObjects {
    init() {
        decodedObjects = []
    }
}

extension DecodedObjects {
    mutating func add(_ object: DecodedObject) {
        decodedObjects.append(object)
    }

    mutating func removeFirst() {
        decodedObjects.removeFirst()
    }
    
    func decodedObject(at index: Int) -> DecodedObject? {
        guard index < decodedObjects.count else { return nil }
        return decodedObjects[index]
    }
    
    func object(at index: Int) -> Decodable? {
        guard index < decodedObjects.count else { return nil }
        return decodedObjects[index].object
    }
    
    func json(at index: Int) -> String? {
        guard index < decodedObjects.count else { return nil }
        return decodedObjects[index].json
    }
}

var decodedObjects = DecodedObjects()
let trackDecodedObjects = true

# <a name="top"></a>Codable Extensions

Improvements to the Codable protocol to allow for:

1. [Decode](#decode) - Object creation from json strings, dictionaries, and from json from a file
2. [Encode](#encode) - JSON and Dictionary representation for Objects
3. [Error Messaging](#error) - Error messages that are descriptive and easy to read

#### Model
Assume a simple PersonModel with a mandatory `name` field of type `String` and an optional `age` field of type `Int?`

	struct PersonModel: Codable {
	    var name: String
	    var age: Int?
	}
	
	extension PersonModel: CustomStringConvertible {
	    var description: String { return "name: \(name)\nage: \(age ?? 0)" }
	}

## <a name="decode"></a>Decode [[top](#top)]
There are 3 main ways of Decoding:

* from a [JSON string](#decode_json)
* from a [Dictionary](#decode_dict)
* from a [file containing JSON](#decode_file)

Each of these all make use of decoding a `Data` object.

### Data [[top](#top)]
	extension Decodable {
	    init?(fromData data: Data) {
	        let object: Self
	        do {
	            object = try JSONDecoder().decode(Self.self, from: data)
	        } catch DecodingError.typeMismatch(_, let context) {
	            print("DecodingError.typeMismatch")
	            print(context)
	            return nil
	        } catch DecodingError.valueNotFound(_, let context) {
	            print("DecodingError.valueNotFound")
	            print(context)
	            return nil
	        } catch DecodingError.keyNotFound(_, let context) {
	            print("DecodingError.keyNotFound")
	            print(context)
	            return nil
	        } catch DecodingError.dataCorrupted(let context) {
	            print("DecodingError.dataCorrupted")
	            print(context)
	            return nil
	        } catch {
	            print("Unexpected decoding error")
	            return nil
	        }
	        
	        self = object
	    }
	}

With this helper in place, these are initializers for the main 3 decoders:

### <a name="decode_json"></a>JSON [[top](#top)]
	extension Decodable {
	    init?(fromJson json: String) {
	        guard let data = json.data(using: .utf8) else {
	            print("Unable to parse json to data object using utf8.\njson:\n\(json)")
	            return nil
	        }
	        
	        self.init(fromData: data)
	    }
	}
	
#### Usage [[top](#top)]
    let goodJson = #"{"name":"Raw Json","age":22}"#
    let person = PersonModel(fromJson: goodJson)

### <a name="decode_dict"></a>Dictionary [[top](#top)]
	typealias JSONDictionary = [String: Any]
	
	func ==(lhs: JSONDictionary, rhs: JSONDictionary ) -> Bool {
	    return NSDictionary(dictionary: lhs).isEqual(to: rhs)
	}
	
	extension Decodable {
	    init?(fromDict dict: JSONDictionary) {
	        guard let data = try? JSONSerialization.data(withJSONObject: dict, options: []) else {
	            print("Unable to parse dict to data object.\ndict:\n\(dict)")
	            return nil
	        }
	                
	        if trackDecodedObjects {
	          guard let object: Self = Self(fromData: data) else { return nil }
	          decodedObjects.add(DecodedObject(json: nil, object: object))
	            self = object
	        } else {
	            self.init(fromData: data)
	        }
	    }
	}

#### Usage [[top](#top)]
    let goodDict: JSONDictionary = ["name":"Predefined Dictionary", "age":33]
    let person = PersonModel(fromDict: goodDict)

### <a name="decode_file"></a>From File [[top](#top)]
	extension Decodable {
	    init?(fromFile file: String) {
	        guard let filePath = Bundle.main.path(forResource: file, ofType: "json") else {
	            print("Could not find file \"\(file).json\"")
	            return nil
	        }
	        
	        guard let json = try? String(contentsOfFile: filePath) else {
	            print("Could not read contents of filepath \"\(filePath)\"")
	            return nil
	        }
	        
	        self.init(fromJson: json)
	    }
	}
	
#### Usage [[top](#top)]
	let person = PersonModel(fromFile: file)

## <a name="encode"></a>Encode [[top](#top)]
Encoding an object has less boilerplate, but can still be improved with the use of a couple of functions for encoding to [JSON](#encode_json) or to a [Dictionary](#encode_dict).

### <a name="encode_json"></a>JSON [[top](#top)]
	extension Encodable {
	    var jsonString: String? {
	        guard let data = try? JSONEncoder().encode(self) else { return nil }
	        return String(data: data, encoding: .utf8)
	    }
	}

#### Usage [[top](#top)]
    let person = PersonModel(name: "Foo Bar", age: 55)
    let json = person.jsonString
    print(json!) // {"name":"Foo Bar","age":55}

### <a name="encode_dict"></a>Dictionary [[top](#top)]
	extension Encodable {
	    var jsonDict: JSONDictionary? {
	        guard let data = try? JSONEncoder().encode(self) else { return nil }
	        guard let dict = try? JSONSerialization.jsonObject(with: data) else { return nil }
	        return dict as? JSONDictionary
	    }
	}

#### Usage [[top](#top)]
    let person = PersonModel(name: "Foo Bar", age: 55)
    let dict = person.jsonDict
    print(dict!) // ["age": 55, "name": Foo Bar]

## <a name="error"></a>Error Messaging [[top](#top)]
With these extensions, the parsing errors are captured and printed in a nice and readable format.

### Examples [[top](#top)]
Attempting to create from good JSON:

	{"name":"Raw Json","age":22}
	Successfully created person:
	name: Raw Json
	age: 22

Attempting to create from bad JSON (type mismatch):

	{"name":"Raw Json","age":"22"}
	DecodingError.typeMismatch
	Context(codingPath: [CodingKeys(stringValue: "age", intValue: nil)], debugDescription: "Expected to decode Int but found a string/data instead.", underlyingError: nil)
	Something went wrong

Attempting to create from bad JSON (value not found):

	{"name":null}
	DecodingError.valueNotFound
	Context(codingPath: [CodingKeys(stringValue: "name", intValue: nil)], debugDescription: "Expected String value but found null instead.", underlyingError: nil)
	Something went wrong

Attempting to create from bad JSON (key not found):

	{"age":22}
	DecodingError.keyNotFound
	Context(codingPath: [], debugDescription: "No value associated with key CodingKeys(stringValue: \"name\", intValue: nil) (\"name\").", underlyingError: nil)
	Something went wrong

Attempting to create from bad JSON (data corrupted):

	{"name":+++}
	DecodingError.dataCorrupted
	Context(codingPath: [], debugDescription: "The given data was not valid JSON.", underlyingError: Optional(Error Domain=NSCocoaErrorDomain Code=3840 "Invalid value around character 8." UserInfo={NSDebugDescription=Invalid value around character 8.}))
	Something went wrong


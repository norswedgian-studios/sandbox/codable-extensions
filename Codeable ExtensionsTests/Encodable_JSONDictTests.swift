//
//  Encodable_JSONDictTests.swift
//  Codeable ExtensionsTests
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Codeable_Extensions

class Encodable_JSONDictTests: XCTestCase {
    let fooBarGood = PersonModel(name: "Foo Bar", age: 55)
    let fooBarNoAge = PersonModel(name: "Foo Bar")
    
    let expectedGoodDict: JSONDictionary = ["name":"Foo Bar", "age":55]
    let expectedNoAgeDict: JSONDictionary = ["name":"Foo Bar"]
}

extension Encodable_JSONDictTests {
    func testJsonDict_good() {
        // Arrange
        let person = fooBarGood
        
        // Act
        let dict = person.jsonDict
        
        // Assert
        XCTAssertNotNil(dict)
        XCTAssertTrue(dict! == expectedGoodDict)
    }
    
    func testJsonDict_noAge() {
        // Arrange
        let person = fooBarNoAge
        
        // Act
        let dict = person.jsonDict
        
        // Assert
        XCTAssertNotNil(dict)
        XCTAssertTrue(dict! == expectedNoAgeDict)
    }
}

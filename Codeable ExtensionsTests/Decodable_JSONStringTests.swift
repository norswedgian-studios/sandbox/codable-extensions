//
//  Decodable_JSONStringTests.swift
//  Codeable ExtensionsTests
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Codeable_Extensions

class Decodable_JSONStringTests: XCTestCase {
    let goodJson = #"{"name":"Raw Json","age":22}"#
    let badJsonTypeMismatch = #"{"name":"Raw Json","age":"22"}"#
    let badJsonValueNotFound = #"{"name":null}"#
    let badJsonKeyNotFound = #"{"age":22}"#
    let badJsonDataCorrupted = #"{"name":+++}"#
}

extension Decodable_JSONStringTests {
    func testInitFromJson_withGoodJson() {
        // Arrange
        let json = goodJson
        
        // Act
        let fooBar = PersonModel(fromJson: json)
        
        // Assert
        XCTAssertNotNil(fooBar)
        XCTAssertEqual(fooBar?.name, "Raw Json")
        XCTAssertEqual(fooBar?.age, 22)
    }
    
    func testInitFromJson_withBadJson_typeMismatch() {
        // Arrange
        let json = badJsonTypeMismatch
        
        // Act
        let fooBar = PersonModel(fromJson: json)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromJson_withBadJson_valueNotFound() {
        // Arrange
        let json = badJsonValueNotFound
        
        // Act
        let fooBar = PersonModel(fromJson: json)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromJson_withBadJson_keyNotFound() {
        // Arrange
        let json = badJsonKeyNotFound
        
        // Act
        let fooBar = PersonModel(fromJson: json)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromJson_withBadJson_dataCorrupted() {
        // Arrange
        let json = badJsonDataCorrupted
        
        // Act
        let fooBar = PersonModel(fromJson: json)
        
        // Assert
        XCTAssertNil(fooBar)
    }
}

//
//  Decodable_JSONFileTests.swift
//  Codeable ExtensionsTests
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Codeable_Extensions

class Decodable_JSONFileTests: XCTestCase {
    let goodFile = "FooBar_good"
    let badFileTypeMismatch = "FooBar_badTypeMismatch"
    let badFileValueNotFound = "FooBar_badValueNotFound"
    let badFileKeyNotFound = "FooBar_badKeyNotFound"
    let badFileDataCorrupted = "FooBar_badDataCorrupted"
}

extension Decodable_JSONFileTests {
    func testInitFromJson_withGoodFile() {
        // Arrange
        let file = goodFile
        
        // Act
        let fooBar = PersonModel(fromFile: file)
        
        // Assert
        XCTAssertNotNil(fooBar)
        XCTAssertEqual(fooBar?.name, "From File")
        XCTAssertEqual(fooBar?.age, 44)
    }
    
    func testInitFromJson_withBadFile_typeMismatch() {
        // Arrange
        let file = badFileTypeMismatch
        
        // Act
        let fooBar = PersonModel(fromFile: file)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromJson_withBadFile_valueNotFound() {
        // Arrange
        let file = badFileValueNotFound
        
        // Act
        let fooBar = PersonModel(fromFile: file)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromJson_withBadFile_keyNotFound() {
        // Arrange
        let file = badFileKeyNotFound
        
        // Act
        let fooBar = PersonModel(fromFile: file)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromJson_withBadFile_dataCorrupted() {
        // Arrange
        let file = badFileDataCorrupted
        
        // Act
        let fooBar = PersonModel(fromFile: file)
        
        // Assert
        XCTAssertNil(fooBar)
    }
}

//
//  Decodable_JSONDictTests.swift
//  Codeable ExtensionsTests
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Codeable_Extensions

class Decodable_JSONDictTests: XCTestCase {
    let goodDict: JSONDictionary = ["name":"Predefined Dictionary", "age":33]
    let badDictTypeMismatch: JSONDictionary = ["name":"Predefined Dictionary", "age":"33"]
    let badDictKeyNotFound: JSONDictionary = ["age":33]
}

extension Decodable_JSONDictTests {
    func testInitFromDict_withGoodDict() {
        // Arrange
        let dict = goodDict
        
        // Act
        let fooBar = PersonModel(fromDict: dict)
        
        // Assert
        XCTAssertNotNil(fooBar)
        XCTAssertEqual(fooBar?.name, "Predefined Dictionary")
        XCTAssertEqual(fooBar?.age, 33)
    }
    
    func testInitFromDict_withBadDict_typeMismatch() {
        // Arrange
        let dict = badDictTypeMismatch
        
        // Act
        let fooBar = PersonModel(fromDict: dict)
        
        // Assert
        XCTAssertNil(fooBar)
    }
    
    func testInitFromDict_withBadDict_keyNotFound() {
        // Arrange
        let dict = badDictKeyNotFound
        
        // Act
        let fooBar = PersonModel(fromDict: dict)
        
        // Assert
        XCTAssertNil(fooBar)
    }
}

//
//  Encodable_JSONStringTests.swift
//  Codeable ExtensionsTests
//
//  Created by Tim Fuqua on 8/10/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Codeable_Extensions

class Encodable_JSONStringTests: XCTestCase {
    let fooBarGood = PersonModel(name: "Foo Bar", age: 55)
    let fooBarNoAge = PersonModel(name: "Foo Bar")
    
    let expectedGoodJson = #"{"name":"Foo Bar","age":55}"#
    let expectedNoAgeJson = #"{"name":"Foo Bar"}"#
}

extension Encodable_JSONStringTests {
    func testJsonString_good() {
        // Arrange
        let person = fooBarGood
        
        // Act
        let json = person.jsonString
        
        // Assert
        XCTAssertNotNil(json)
        XCTAssertEqual(json, expectedGoodJson)
    }
    
    func testJsonString_noAge() {
        // Arrange
        let person = fooBarNoAge
        
        // Act
        let json = person.jsonString
        
        // Assert
        XCTAssertNotNil(json)
        XCTAssertEqual(json, expectedNoAgeJson)
    }
}
